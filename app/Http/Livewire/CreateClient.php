<?php

namespace App\Http\Livewire;

use App\Models\Client;
use Livewire\Component;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CreateClient extends Component {

    public $name;

    protected $rules = ['name' => ['required', 'min:3', 'max:100']];

    public function render() {
        return view('livewire.create-client');
    }

    public function createClient() {
        if (Auth::guest() || Auth::user() -> isDeveloper()) {
            abort(Response::HTTP_FORBIDDEN);
        }

        $this -> validate();

        $newClient = Client::create([
            'name' => $this -> name
        ]);

        $this -> emit('notificationSuccess', 'User was successfully created!');
        $this -> emit('clientCreated');
    }

}
