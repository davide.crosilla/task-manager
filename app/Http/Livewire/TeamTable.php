<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class TeamTable extends Component {

    use WithPagination;

    protected $listeners = ['userCreated'];

    public function render() {
        $team = User::with(['role'])
            -> withCount('projects')
            -> paginate(10);

        return view('livewire.team-table', [
            'team' => $team
        ]);
    }

    public function userCreated() {
        $this -> gotoPage(User::with(['role'])
            -> withCount('projects')
            -> paginate(10) -> lastPage());
    }

}
