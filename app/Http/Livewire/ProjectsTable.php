<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Project;
use Illuminate\Support\Facades\Auth;

class ProjectsTable extends Component {

    use WithPagination;

    protected $listeners = ['projectCreated'];

    public function render() {
        if (Auth::user() -> isProjectManager()) {
            $projects = Project::with(['client', 'status'])
                -> withCount('users')
                -> withCount('tasks')
                -> paginate(10);
        } else {
            $projects = Project::with(['client', 'status'])
                -> leftJoin('user_projects', 'projects.id', '=', 'user_projects.project_id')
                -> where('user_projects.user_id', Auth::user() -> id)
                -> withCount('users')
                -> withCount('tasks')
                -> paginate(10);
        }

        return view('livewire.projects-table', ['projects' => $projects]);
    }

    public function projectCreated() {
        $this -> gotoPage(Project::with(['client', 'status'])
            -> withCount('users')
            -> withCount('tasks')
            -> paginate(10) -> lastPage());
    }

}
