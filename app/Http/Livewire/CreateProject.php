<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Client;
use App\Models\Project;
use App\Models\User;
use App\Models\UserProject;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CreateProject extends Component {

    public $title;
    public $client;
    public $description;
    public $users;

    protected $rules = [
        'title' => ['required', 'min:3', 'max:100', 'unique:projects'],
        'client' => ['required', 'exists:clients,id'],
        'description' => ['required', 'min:10'],
        'users' => ['required']
    ];


    public function render() {
        $clients = Client::all();
        $users = User::all();

        return view('livewire.create-project', [
            'clients' => $clients,
            'allUsers' => $users
        ]);
    }

    public function createProject() {
        if (Auth::guest() || Auth::user() -> isDeveloper()) {
            abort(Response::HTTP_FORBIDDEN);
        }

        $this -> validate();

        $newProject = Project::create([
            'title' => $this -> title,
            'client_id' => $this -> client,
            'status_id' => 3,
            'description' => $this -> description
        ]);

        foreach ($this -> users as $i => $id) {
            UserProject::create([
                'user_id' => $id,
                'project_id' => $newProject -> id
            ]);
        }

        $this -> emit('notificationSuccess', 'Project was successfully created!');
        $this -> emit('projectCreated');

    }

}
