<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model {

    use HasFactory;

    protected $fillable = ['title', 'description', 'client_id', 'status_id'];

    public function users() {
        return $this -> hasMany(UserProject::class);
    }

    public function client() {
        return $this -> belongsTo(Client::class);
    }

    public function tasks() {
        return $this -> hasMany(Task::class);
    }

    public function status() {
        return $this -> belongsTo(Status::class);
    }

}
