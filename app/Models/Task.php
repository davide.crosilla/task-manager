<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model {

    protected $guarded = [];

    use HasFactory;

    public function project() {
        return $this -> belongsTo(Project::class);
    }

    public function status() {
        return $this -> belongsTo(Status::class);
    }

    public function priority() {
        return $this -> belongsTo(Priority::class);
    }

    public function createdBy() {
        return $this -> belongsTo(User::class, 'created_by', 'id');
    }

    public function assignedTo() {
        return $this -> belongsTo(User::class, 'assigned_to', 'id');
    }

}
