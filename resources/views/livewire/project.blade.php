<x-app-layout>

    <div class="mb-2">
        <div class="w-full flex flex-row items-center justify-between mt-6">
            <h1 class="font-bold text-2xl">
                <a
                    class="w-fit-content flex flex-row items-center px-4 py-1 rounded-xl text-cyan-400 hover:text-cyan-500 hover:bg-cyan-200 hover:bg-opacity-20 transition"
                    href="{{ route('projects') }}"
                >
                    <svg class="h-6 w-6 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 16l-4-4m0 0l4-4m-4 4h18" />
                    </svg>
                    {{ $project -> title }}
                </a>
            </h1>

            <livewire:change-status :item="$project" :statuses="$statuses" :css="'text-base py-2'" />
        </div>

        <p class="text-base mt-4">{{ $project -> description }}</p>

        <div class="w-full grid grid-cols-3 gap-4">

            <div class="w-full col-span-2">
                <div
                    class="flex flex-row items-center justify-between mt-4"
                    x-data="{ show: false }"
                    x-init="
                        Livewire.on('taskCreated', () => {
                            show = false;
                        });
                    "
                >
                    <h3 class="text-xl font-semibold">Tasks – {{ $tasks -> count() }}</h3>
                    @if (auth() -> user() -> isProjectManager())
                        <a
                            @click="show = true"
                            class="cursor-pointer ml-auto mr-0 flex flex-row items-center px-4 py-2 rounded-xl font-semibold text-cyan-400 hover:text-cyan-500 hover:bg-cyan-200 hover:bg-opacity-20 transition"
                        >
                            <svg class="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z" clip-rule="evenodd" />
                            </svg>
                            New task
                        </a>

                        <div x-cloak>
                            <livewire:create-task :project="$project" />
                        </div>
                    @endif

                </div>

                <div class="my-4 grid grid-cols-2 gap-4">
                    @if (auth() -> user() -> can('viewAny', $project))
                        @if (count($tasks))
                            @foreach ($tasks as $task)
                                @if (auth() -> user() -> can('view', $task))
                                    <livewire:task-view :task="$task" :statuses="$statuses" />
                                @endif
                            @endforeach
                        @else
                            <p class="w-max">There are no tasks yet.</p>
                        @endif
                    @else
                        <p class="w-max">There are no tasks assigned to you.</p>
                    @endif
                </div>

            </div>

            <div class="w-full">
                <div
                    class="flex flex-row items-center justify-between mt-4"
                    x-data="{ show: false }"
                >
                    <h3 class="text-xl font-semibold">Team – {{ $projectMembers -> count() }}</h3>
                    @if (auth() -> user() -> isProjectManager())
                        <a
                            @click="show = true"
                            class="cursor-pointer ml-auto mr-0 flex flex-row items-center px-4 py-2 rounded-xl font-semibold text-cyan-400 hover:text-cyan-500 hover:bg-cyan-200 hover:bg-opacity-20 transition"
                        >
                            <svg class="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z" clip-rule="evenodd" />
                            </svg>
                            Assign new member
                        </a>
                        <div class="relative">
                            <livewire:assign-member-to-project :project="$project" :projectMembers="$projectMembers" />
                        </div>
                    @endif

                </div>

                <div class="mt-4 grid grid-cols-1 gap-4">
                    @foreach ($projectMembers as $projectMember)
                        <x-small-user-view :user="$projectMember -> user" />
                    @endforeach
                </div>

            </div>

        </div>
    </div>
</x-app-layout>
