<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20" />
            </a>
        </x-slot>

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <x-input :name="'email'" :type="'email'" class="mt-0" />

            <x-input :name="'password'" :type="'password'" />

            <!-- Remember Me -->
            <div class="flex flex-row items-center justify-between mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="transition rounded border-gray-300 text-cyan-400 shadow-sm focus:border-cyan-200 focus:ring focus:ring-cyan-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
                <x-button class="ml-3">
                    {{ __('Log in') }}
                </x-button>
            </div>

        </form>
    </x-auth-card>
</x-guest-layout>
