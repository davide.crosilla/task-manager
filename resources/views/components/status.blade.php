@props(['item' => '', 'status' => '', 'priority' => null, 'css' => 'text-xs py-1'])

<div class="w-max {{ $item -> status -> styles ?? $status -> styles }} rounded-full">
    @if ($priority)
        <span class="min-w-max px-4 inline-flex {{ $css }} leading-5 rounded-full {{ $priority -> styles }}">
            {{ $priority -> name }}
        </span>
    @endif
    <span class="min-w-max px-4 inline-flex {{ $css }} leading-5 font-semibold rounded-full {{ $item -> status -> styles ?? $status -> styles }} @if ($priority) pl-2 @endif">
        {{ $item -> status -> name ?? $status -> name }}
    </span>
</div>

