@props(['size' => 'h-8 w-8', 'src'])

<img class="{{ $size }} rounded-full" src="{{ $src }}" alt="Avatar">
