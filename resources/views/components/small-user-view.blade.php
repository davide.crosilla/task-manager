@props(['user'])

<div class="p-4 whitespace-nowrap flex flex-row items-center space-x-4 bg-white rounded-xl">
    <x-avatar :src="$user -> getAvatar()" :size="'w-12 h-12'" />
    <div class="flex flex-col">
        <span class="text-base font-semibold text-gray-900">{{ $user -> name }} – {{ $user -> role -> name }}</span>
        <span class="text-sm text-gray-500">{{ $user -> email}}</span>
    </div>
</div>
