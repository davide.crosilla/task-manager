<button
    {{ $attributes -> merge([
        'type' => 'submit',
        'class' => 'inline-flex justify-center rounded-xl border border-transparent px-4 py-2 bg-cyan-400 text-base font-semibold text-white hover:bg-cyan-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-cyan-400 transition']) }}
>
    {{ $slot }}
</button>
