@props(['message' => '', 'icon' => ''])

<div
    class="z-20 flex justify-between items-center max-w-xs sm:max-w-sm w-full fixed bottom-0 right-0 bg-white rounded-xl border px-4 py-3 mx-2 sm:mx-6 my-8"
    x-cloak
    x-data="{
        show: false,
        displayMessage: '{{ $message }}',
        icon: '',
        showNotification(message, icon) {
            this.show = true;
            this.displayMessage = message;
            this.icon = icon,
            setTimeout(() => {
                this.show = false;
            }, 5000);
        }
    }"
    x-init="
        Livewire.on('notificationSuccess', message => {
            showNotification(message, 'success')
        });
        Livewire.on('notificationError', message => {
            showNotification(message, 'error')
        });
        if (displayMessage !== '') {
            $nextTick(() => showNotification(displayMessage))
        }
    "
    x-show="show"
    x-transition:enter="transition ease-out duration-150"
    x-transition:enter-start="opacity-0 transform translate-x-8"
    x-transition:enter-end="opacity-100 transform translate-x-0"
    x-transition:leave="transition ease-in duration-150"
    x-transition:leave-start="opacity-100 transform translate-x-0"
    x-transition:leave-end="opacity-0 transform translate-x-8"
    @keydown.escape.window="show = false"
>
    <div class="flex items-center">
        <svg x-show="[icon, '{{ $icon }}'].includes('success')" class="h-6 w-6 text-green-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <svg x-show="[icon, '{{ $icon }}'].includes('error')" class="h-6 w-6 text-red-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
        <span class="font-semibold text-gray-500 text-sm sm:text-base ml-2" x-text="displayMessage"></span>
    </div>
    <div class="flex-shrink-0 flex items-center justify-center h-10 w-10 rounded-full hover:bg-red-100 transition sm:mx-0 sm:h-10 sm:w-10 cursor-pointer" @click="show = false">
        <svg class="h-5 w-5 text-red-600" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
        </svg>
    </div>
</div>
