<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Client;
use App\Models\Project;
use App\Models\Task;
use App\Models\Role;
use App\Models\Priority;
use App\Models\Status;
use App\Models\UserProject;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {

        // ROLES
        Role::factory() -> create(['name' => 'Project Manager']);
        Role::factory() -> create(['name' => 'Developer']);

        // PRIORITIES
        Priority::factory() -> create(['name' => 'Low', 'styles' => 'bg-green-500 text-white font-bold']);
        Priority::factory() -> create(['name' => 'Medium', 'styles' => 'bg-yellow-400 text-white font-bold']);
        Priority::factory() -> create(['name' => 'High', 'styles' => 'bg-red-500 text-white font-bold']);

        // STATUSES
        Status::factory() -> create(['name' => 'Backlog', 'styles' => 'bg-indigo-100 text-indigo-700']);
        Status::factory() -> create(['name' => 'To do', 'styles' => 'bg-red-100 text-red-700']);
        Status::factory() -> create(['name' => 'In progress', 'styles' => 'bg-yellow-100 text-yellow-700']);
        Status::factory() -> create(['name' => 'Done', 'styles' => 'bg-green-100 text-green-700']);

        // USERS
        User::factory() -> create([
            'role_id' => 1,
            'name' => 'Admin',
            'email' => 'admin@email.com'
        ]);
        User::factory(2) -> create(['role_id' => 1]);
        User::factory(12) -> create(['role_id' => 2]);

        // CLIENTS
        Client::factory(15) -> create();

        // PROJECTS
        foreach (range(1, 15) as $clientID) {
            Project::factory(2) -> create(['client_id' => $clientID, 'status_id' => rand(3, 4)]);
        }

        // ASSIGNING USERS TO PROJECTS
        foreach (range(1, 30) as $projectID) {
            $random = rand(1, 3);
            UserProject::factory() -> create([
                'user_id' => $random,
                'project_id' => $projectID
            ]);
            foreach (range(1, 3) as $value) {
                $random = rand(4, 15);
                while (UserProject::where('project_id', $projectID) -> where('user_id', $random) -> exists()) {
                    $random = rand(4, 15);
                }
                UserProject::factory() -> create([
                    'user_id' => $random,
                    'project_id' => $projectID
                ]);
                foreach (range(1, 3) as $v) {
                    $random = rand(4, 15);
                    while (!UserProject::where('project_id', $projectID) -> where('user_id', $random) -> exists()) {
                        $random = rand(4, 15);
                    }
                    Task::factory() -> create([
                        'created_by' => $value,
                        'assigned_to' => $random,
                        'project_id' => $projectID,
                        'priority_id' => rand(1, 3)
                    ]);
                }
            }
        }

    }
}
